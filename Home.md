# Krystian Kisielewski

Wybrany zbiór danych: [Book-Crossing Dataset](http://www2.informatik.uni-freiburg.de/~cziegler/BX/)

## Zaliczenie:

- [zadanie GEO](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie0)

## Egzamin:

- [Agregacje](https://bitbucket.org/nosql2017/nosql/wiki/Egzamin)

## Inne:

- [Aggregation Pipeline](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie1)
- [zad 1 (stare)](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie1.old)

### Informacje o komputerze, na którym były wykonywane obliczenia:

| Nazwa                 | Wartość    |
|-----------------------|------------|
| System operacyjny     | Windows 7 64-bit |
| Procesor              | i7-3630QM 4C/8T 2.4-3.4 GHz |
| Pamięć                | 8 GB DDR3 1600 MHz dual channel |
| Dysk                  | HDD 5400 RPM |
| Bazy danych           | mongoDB 3.4.2, PostgreSQL 9.6.2, elasticsearch 5.2.2 |
| Język programowania   | Python 3.6.0 64-bit |
| Biblioteki            | pymongo 3.4.0, psycopg2 2.7.1, requests 2.13.0, elasticsearch 1.9.0, elasticsearch_dsl 5.2.0 |