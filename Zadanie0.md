# Spis treści:

1. [Informacje o danych](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie0#markdown-header-dane-link)
2. [mongoDB](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie0#markdown-header-rozwiazanie-wykorzystujace-baze-mongodb)
3. [elasticsearch](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie0#markdown-header-rozwiazanie-wykorzystujace-baze-elasticsearch)
4. [Porównanie baz](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie0#markdown-header-porownanie-najwazniejszych-wynikow)
5. [Wyniki](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie0#markdown-header-wyniki)
6. [Podsumowanie](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie0#markdown-header-podsumowanie)

- - -

# Dane: [link](http://data.cyc.opendata.arcgis.com/datasets/3f0de3422ec343e498d6d7699c114858_3.geojson)

Dane zawierają informacje o zabytkach w okolicach miasta York i skupiają w sobie informacje o **2656** obiektach, a sam plik z danymi ma wielkość **884 kB**.

## Przykładowy obiekt:

```json
{
    "_id" : ObjectId("58d57c772b4070101c58a29d"),
    "type" : "Feature",
    "properties" : {
        "OBJECTID" : 145053,
        "MONUID" : "MYO1292",
        "RECORDTYPE" : "BLD",
        "NAME" : "28 Lord Mayor's Walk",
        "MONTYPE" : "HOUSE",
        "GATEWAYURL" : "http://www.heritagegateway.org.uk/Gateway/Results_Single.aspx?resourceID=1003&uid=MYO1292"
    },
    "geometry" : {
        "type" : "Point",
        "coordinates" : [
            -1.0793090497137183,
            53.96369207088339
        ]
    }
}
```

- - -

# Rozwiązanie wykorzystujące bazę **mongoDB**:

Rozwiązanie zadania zostało opracowane przy użyciu języka Python w wersji 3.6.0, z wykorzystaniem biblioteki pymongo w wersji 3.4.0 oraz Requests w wersji 2.13.0. Automatyzuje on wszystkie czynności, a jego kod znajduje się [tutaj](https://bitbucket.org/nosql2017/nosql/src/master/zad0/scripts/mongo.py).

Na działanie skryptu składają się następujące elementy:

1. Pobranie pliku geojson z danymi ze strony podanej powyżej
2. Nawiązanie połączenia z bazą danych mongoDB
3. Usunięcie (jeśli istnieje) kolekcji *monuments*
4. Wczytanie i sparsowanie uprzednio pobranego pliku z danymi zabytków
5. Wstawienie danych do kolekcji *monuments*
6. Zliczenie wstawionych do kolekcji dokumentów
7. Utworzenie indeksu na polu *geometry*
8. Wykonanie kilku zapytań wykorzystujących współrzędne geograficzne
9. Zapisanie wyników wspomnianych zapytań do plików w formacie geojson

Pobieranie danych, usuwanie starych danych z bazy, wstawianie danych do bazy, zliczanie dodanych dokumentów, zapytania i zapisywanie wyników zostały opatrzone pomiarem czasu wykonania. Należy zauważyć, że pobieranie kolejnych zabytków, w celu zmniejszenia zużycia pamięci i umożliwienia współpracy z dużymi zbiorami danych, zostało zrealizowane za pomocą generatora, dlatego też czas pobrania kolejnych obiektów jest wliczony do czasu wstawiania ich do bazy. Nie są to jednak wartość, która miałaby znaczący wpływ na pomiar czasu wypełniania bazy danymi.

| Czynność | Czas trwania |
|----------|--------------|
| Pobieranie danych | 1.2 s |
| Czyszczenie bazy danych | 47 ms |
| Wstawianie danych | 392.02 ms |
| Zliczanie dokumentów | 1 ms\* |
| Pierwsze zapytanie\*\* | 1 ms\* |
| Zapis rezultatów zapytania | 45 ms |
| Drugie zapytanie\*\* | 1 ms |
| Zapis rezultatów zapytania | 8 ms |
| Trzecie zapytanie\*\* | 1 ms\* |
| Zapis rezultatów zapytania | 59 ms |

\*Pomimo wielokrotnego powtarzania wykonania skryptu i wyłączenia zaokrąglania wyników, niektóre zadania zawsze wykonywały się w 0 ms, czyli poniżej dokładności pomiaru czasu. Ponieważ wykonanie tych zadań jakiś ułamek czasu trwało, zdecydowałem się zaokrąglić je do pełnej milisekundy.

\*\*Treść zapytań znajduje się na stronie z wynikami, do której link znajduje się w sekcji [Wyniki](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie0#markdown-header-wyniki).

### Wykorzystanie zasobów:

| Zasób | Wykorzystanie |
|-------|---------------|
| CPU | 1%|
| RAM | 22 MB|

# Rozwiązanie wykorzystujące bazę **elasticsearch**:

Rozwiązanie zadania zostało opracowane przy użyciu języka Python w wersji 3.6.0, z wykorzystaniem bibliotek Requests w wersji 2.13.0, elasticsearch w wersji 1.9.0 oraz elasticsearch_dsl w wersji 5.2.0. Automatyzuje on wszystkie czynności, a jego kod znajduje się [tutaj](https://bitbucket.org/nosql2017/nosql/src/master/zad0/scripts/elastic.py).

Na działanie skryptu składają się następujące elementy:

1. Pobranie pliku geojson z danymi ze strony podanej powyżej
2. Usunięcie (jeśli istnieje) indeksu *location*
3. Wczytanie i sparsowanie uprzednio pobranego pliku z danymi zabytków
4. Utworzenie mappingu dla indeksu *location*
5. Wstawienie danych do bazy elasticsearch (/location/monument)
6. Zliczenie zabytków znajdujących się w bazie
7. Wykonanie kilku zapytań wykorzystujących współrzędne geograficzne

Pobieranie danych, usuwanie starych danych z bazy, tworzenie mappingu, wstawianie danych do bazy, zliczanie dodanych zabytków i poszczególne zapytania zostały opatrzone pomiarem czasu wykonania. Należy zauważyć, że pobieranie kolejnych zabytków, w celu zmniejszenia zużycia pamięci i umożliwienia współpracy z dużymi zbiorami danych, zostało zrealizowane za pomocą generatora, dlatego też czas pobrania kolejnych obiektów jest wliczony do czasu wstawiania ich do bazy. Nie są to jednak wartość, która miałaby znaczący wpływ na pomiar czasu wypełniania bazy danymi.

| Czynność | Czas trwania |
|----------|--------------|
| Pobieranie danych | 1.2 s |
| Czyszczenie bazy danych | 137.01 ms |
| Dodanie mappingu\* | 1.76 s |
| Wstawianie danych | 1.95 s\*\* |
| Zliczanie dokumentów | 14.0 ms |
| Pierwsze zapytanie\*\*\* | 4 ms |
| Drugie zapytanie\*\*\* | 3 ms |
| Trzecie zapytanie\*\*\* | 5 ms |


\*Dodanie mappingu jest konieczne, ponieważ odgadywanie typu wstawianych danych w elasticsearch nie jest w stanie rozpoznać obiektu *geo_point*. [[źródło]](https://www.elastic.co/guide/en/elasticsearch/guide/master/geopoints.html)

\*\*Wstawianie danych do bazy jest powodem, dla którego konieczne stało się wykorzystanie dwóch bibliotek: wysoko- i niskopoziomowej, ponieważ biblioteka wysokopoziomowa nie oferuje możliwości wstawienia wielu elementów na raz. Czas dodania 2656 zabytków jednego po drugim wyniósł 457.44 s i był nieakceptowalnie wysoki.

\*\*\*Treść zapytań znajduje się na stronie z wynikami, do której link znajduje się w sekcji [Wyniki](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie0#markdown-header-wyniki).


### Wykorzystanie zasobów:

| Zasób | Wykorzystanie |
|-------|---------------|
| CPU | 2-6% |
| RAM | 24 MB |

### Zabytki w bazie **elasticsearch**:

| health | status | index | uuid | pri | rep | docs.count | docs.deleted | store.size | pri.store.size |
|--------|--------|-------|------|-----|-----|------------|--------------|------------|----------------|
| yellow\* | open | location | cIn... | 5 | 1 | 2656 | 0 | 1.1mb | 1.1mb |

\*Wyjaśnienie statusu *yellow*:
> (...) Elasticsearch by default created one replica for this index. Since we only have one node running at the moment,  that one replica cannot yet be allocated (for high availability) until a later point in time when another node joins
> the cluster. Once that replica gets allocated onto a second node, the health status for this index will turn to green.

- - -

# Porównanie najważniejszych wyników

W poniższych tabelach przedstawiających zestawienie wyników dla baz **mongoDB** i **elasticsearch** pogrubione zostały te czasy, które dla danej czynności okazały się być mniejsze.

### Import danych:

| Czynność | mongoDB | elasticsearch |
|----------|---------|---------------|
| Czyszczenie bazy danych | **47 ms** | 137.01 ms |
| Dodanie mappingu | ***nie dotyczy*** | 1.76 s |
| Wstawianie danych | **392.02 ms** | 1.95 s |
| Zliczanie dokumentów | **1 ms** | 14.0 ms |

### Zapytania:

| Zapytanie | Liczba zwracanych zabytków | mongoDB | elasticsearch | Mapka |
|-----------|----------------------------|---------|---------------|-------|
| Pierwsze\* | 383 | **1 ms** | 4 ms | [link](https://bitbucket.org/nosql2017/nosql/src/629169c2371707ca1104117e8a1642d247fc8ff6/zad0/output1.geojson?at=master&fileviewer=file-viewer-for-bitbucket-cloud%3Ageo-viewer) |
| Drugie\* | 4 | **1 ms** | 3 ms| [link](https://bitbucket.org/nosql2017/nosql/src/629169c2371707ca1104117e8a1642d247fc8ff6/zad0/output2.geojson?at=master&fileviewer=file-viewer-for-bitbucket-cloud%3Ageo-viewer) |
| Trzecie\* | 445 | **1 ms** | 5 ms | [link](https://bitbucket.org/nosql2017/nosql/src/629169c2371707ca1104117e8a1642d247fc8ff6/zad0/output3.geojson?at=master&fileviewer=file-viewer-for-bitbucket-cloud%3Ageo-viewer) |

\*Treść zapytań znajduje się na stronie z wynikami, do której link znajduje się w sekcji [Wyniki](https://bitbucket.org/nosql2017/nosql/wiki/Zadanie0#markdown-header-wyniki).

### Wykorzystanie zasobów:

W tabelce przedstawiającej zestawienie zużycia zasobów dla baz **mongoDB** i **elasticsearch** pogrubione zostały te wartości, które okazały się być mniejsze.

| Zasób | mongoDB| elasticsearch |
|-------|--------|---------------|
| CPU | **1%** | 2-6% |
| RAM | **22 MB** | 24 MB |

Należy mieć na uwadze, że ze względu na krótki czas działania programów oraz obecności funkcji Intel Turbo Boost 2.0™, pomiar wykorzystania mocy procesora jest daleki od ideału.

Nie można też zapominać, że samo uruchomienie, na standardowych ustawieniach, bazy *elasticsearch* pochłania ponad **2 GB RAM**, podczas gdy proces serwera bazy mongoDB po uruchomieniu zajmuje niecałe **83 MB RAM**. Z powodu trudności w pomiarze ile pamięci jest aktywnie wykorzystywane przez bazę *elasticsearch*, postanowiłem w tym zadaniu nie brać tych wartości pod uwagę.
- - -

# Wyniki

## [Strona z mapkami](https://nosql2017.github.io/maps/)

- - -

# Podsumowanie

Dla **każdego** zadania baza *mongoDB* uzyskuje lepsze wyniki (krótszy czas wykonania) od bazy *elasticsearch*.

- - -

# Zadanie wykonane ręcznie:

### Import:
```
(Measure-Command {mongoimport -c monuments --file '.\Dane\Archaeological_Monuments.geojson' --jsonArray}).TotalSeconds
```
Czas importu: 0.5652078 s


### Tworzenie indeksu:
```
db.monuments.ensureIndex({"geometry" : "2dsphere"})
```

### Zapytania:
Jesteśmy niedaleko Bettys Café Tea Rooms w Yorku. Jakie zabytki znajdują się nie dalej niż 300 m od nas?
```
mongoexport -c monuments -q "{ geometry : {$near : {$geometry : { type : 'Point', coordinates : [-1.084012, 53.960160 ] }, $maxDistance : 300}}}" -o output1.geojson --jsonArray --pretty
```
[Wyniki (mapa)](https://bitbucket.org/nosql2017/nosql/src/629169c2371707ca1104117e8a1642d247fc8ff6/zad0/output1.geojson?at=master&fileviewer=file-viewer-for-bitbucket-cloud%3Ageo-viewer)

- - -

Jakie zabytki znajdują się w kwadracie rozpinającym się pomiędzy 
National Railway Museum a York Observatory?
```
mongoexport -c monuments -q "{ geometry : {$geoWithin: { $box: [[ -1.096160, 53.960636 ],[ -1.087936, 53.961244 ]]}}}" -o output2.geojson --jsonArray --pretty
```
[Wyniki (mapa)](https://bitbucket.org/nosql2017/nosql/src/629169c2371707ca1104117e8a1642d247fc8ff6/zad0/output2.geojson?at=master&fileviewer=file-viewer-for-bitbucket-cloud%3Ageo-viewer)

- - -

Jesteśmy niedaleko Bettys Café Tea Rooms w Yorku. Jakie zabytki znajdują się co najmniej 500 m od nas, ale nie dalej niż 1 km?
```
mongoexport -c monuments -q "{ geometry : {$near : {$geometry : { type : 'Point', coordinates : [-1.084012, 53.960160 ] }, $minDistance : 500, $maxDistance : 1000}}}" -o output3.geojson --jsonArray --pretty
```
[Wyniki (mapa)](https://bitbucket.org/nosql2017/nosql/src/629169c2371707ca1104117e8a1642d247fc8ff6/zad0/output3.geojson?at=master&fileviewer=file-viewer-for-bitbucket-cloud%3Ageo-viewer)

### [Powrót do strony głównej](https://bitbucket.org/nosql2017/nosql/wiki/Home)