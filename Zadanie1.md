# Zadanie 1

## Dane: [Superuser](https://archive.org/download/stackexchange/superuser.com.7z) (656 MB)

Dane zawierają informacje o postach z portalu superuser.com.
W zadaniu wykorzystano informacje o użytkownikach i o napisanych przez nich postach.

| Dane | Liczba rekordów | Rozmiar pliku |
|------|-----------------|---------------|
| users | 500264 | 165 MB |
| posts | 859690 | 964 MB |

Aby nie pobierać niepotrzebnych danych, pliki *users.xml* oraz *posts.xml* umieściłem na dropboksie.

- [users.xml](http://dl.dropbox.com/s/7ddq578gs6z9sre/users.xml) (165 MB)
- [posts.xml](http://dl.dropbox.com/s/ewmwofxawy2airc/posts.xml) (964 MB)

- - -

# Pobieranie danych

Pobieranie danych zostało zrealizowane za pomocą skryptu napisanego w języku Python w wersji 3.6.0.
Kod programu dostępny jest [tutaj](https://bitbucket.org/nosql2017/nosql/src/master/zad1/scripts/download.py).

![](https://bytebucket.org/nosql2017/nosql/raw/1eaf9188b90ecdc70bd901d2587e385608ccee6e/zad1/images/downloading.gif?token=377b762056f0bfe89bab7e702ed174175e4f6505)

| Nazwa pliku | Czas pobierania |
|-------------|-----------------|
| users.xml | 8.46 s |
| posts.xml | 59.72 s |

# Konwersja formatu danych z XML do CSV

Konwersja plików z danymi z fromatu XML na format CSV została wykonana za pomocą skryptu napisanego w języku Python w wersji 3.6.0. Kod programu dostępny jest [tutaj](https://bitbucket.org/nosql2017/nosql/src/master/zad1/scripts/xml2csv.py).

Poszczególne pola zostały rozdzielone średnikiem i otoczone cudzysłowem.

| Dane wejściowe | Wielkość wejścia | Czas konwersji | Dane wyjściowe | Wielkość wyjścia |
|----------------|------------------|----------------|----------------|------------------|
| users.xml | 165 MB | 10.52 s | users.csv | 106 MB |
| posts.xml | 964 MB | 35.45 s | posts.csv | 751 MB |