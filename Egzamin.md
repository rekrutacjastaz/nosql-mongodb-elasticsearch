# Projekt Aggregation Pipeline (egzamin)

## Krystian Kisielewski

### Informacje o komputerze, na którym były wykonywane obliczenia

| Nazwa                 | Wartość    |
|-----------------------|------------|
| System operacyjny     | Windows 7 64-bit |
| Procesor              | i7-3630QM 4C/8T 2.4-3.4 GHz |
| Pamięć                | 8 GB DDR3 1600 MHz dual channel |
| Dysk                  | HDD 5400 RPM |
| Bazy danych           | mongoDB 3.4.2, PostgreSQL 9.6.2 |
| Język programowania   | Python 3.6.0 64-bit |
| Biblioteki            | pymongo 3.4.0, psycopg2 2.7.1, requests 2.13.0 |


## Dane: [Superuser](https://archive.org/download/stackexchange/superuser.com.7z) (656 MB)

Dane zawierają informacje o postach z portalu [superuser.com](https://superuser.com).
W zadaniu wykorzystano informacje o użytkownikach i o napisanych przez nich postach.

| Dane  | Liczba rekordów | Rozmiar pliku |
|-------|-----------------|---------------|
| users | 500264          | 165 MB        |
| posts | 859690          | 964 MB        |

Aby nie pobierać niepotrzebnych danych, pliki *users.xml* oraz *posts.xml* umieściłem na dropboksie.

- [users.xml](http://dl.dropbox.com/s/7ddq578gs6z9sre/users.xml) (165 MB)
- [posts.xml](http://dl.dropbox.com/s/ewmwofxawy2airc/posts.xml) (964 MB)

### Przykładowy użytkownik

```json
 {
   "Id": 3565,
   "AboutMe": "<p>Vice President of Engineering at Pramata, an amazing B2B SaaS company providing the essential
               intelligence from contracts and billing systems to help retain and grow companies most valuable 
               customer accounts.<br>Fan of tennis and Rock Climbing.</p>",
   "Reputation": 101,
   "LastAccessDate": "2016-10-26T17:37:08.053",
   "UpVotes": 24,
   "DisplayName": "Brian Fisher",
   "AccountId": 18908,
   "Age": 42,
   "ProfileImageUrl": "",
   "DownVotes": 0,
   "WebsiteUrl": "http://www.pramata.com",
   "Location": "San Francisco, CA",
   "CreationDate": "2009-07-24T00:29:05.050",
   "Views": 3
 }
```

- - -

## Agregacje

1. 3 posty z najwyższą oceną
2. Liczba postów napisanych między 17 kwietnia a 17 maja 2016 r.
3. Użytkownicy z liczbą postów między 311 a 341

- - -

## Prezentacja

[prezentacja.pdf](https://bitbucket.org/nosql2017/nosql/src/e13a5fb695f347cdaf1859173a47c29326f869c1/prezentacja.pdf?at=master&fileviewer=file-viewer-for-bitbucket-cloud%3Apdf-viewer)