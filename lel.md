```js
{
  "stages" : [
    {
      "$cursor" : {
        "query" : {

        },
        "fields" : {
          "OwnerUserId" : 1,
          "X.AccountId" : 1,
          "_id" : 0
        },
        "queryPlanner" : {
          "plannerVersion" : 1,
          "namespace" : "test.posts",
          "indexFilterSet" : false,
          "parsedQuery" : {

          },
          "winningPlan" : {
            "stage" : "EOF"
          },
          "rejectedPlans" : [ ]
        }
      }
    },
    {
      "$lookup" : {
        "from" : "users",
        "as" : "X",
        "localField" : "OwnerUserId",
        "foreignField" : "AccountId"
      }
    },
    {
      "$group" : {
        "_id" : "$X.AccountId",
        "posts" : {
          "$sum" : {
            "$const" : 1
          }
        }
      }
    },
    {
      "$match" : {
        "posts" : {
          "$gte" : 311,
          "$lte" : 341
        }
      }
    },
    {
      "$sort" : {
        "sortKey" : {
          "posts" : -1
        }
      }
    },
    {
      "$project" : {
        "_id" : true,
        "posts" : true
      }
    }
  ],
  "ok" : 1
}
```