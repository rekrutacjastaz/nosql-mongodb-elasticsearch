# Zadanie 2

Do wykonania tego zadania posłużyłem się tymi samymi danymi co w zadaniu 1.

Dane: [Sports on Stackexchange](https://archive.org/download/stackexchange/sports.stackexchange.com.7z)

Dane zawierają informacje o postach dotyczących sportu z portalu stackexchange.com.
W zadaniu wykorzystano informacje o użytkownikach i o napisanych przez nich postach.

| Dane | Liczba rekordów |
|------|---------------|
| users | 6423 |
| posts | 9366 |

## PostgreSQL

Część zadania opierająca się o bazę PostgreSQL została wykonana w języku Python 
w wersji 3.6.0 z wykorzystaniem biblioteki psycopg2 w wersji 2.7.1. 

Kod rozwiązania dostępny [tutaj](https://bitbucket.org/nosql2017/nosql/src/master/zad2/scripts/postgre.py). 
*Hasło do bazy danych zostało celowo zamaskowane.

Skrypt składa się z następujących części:

1. Na wstępie z bazy danych usuwane są (o ile istnieją) tabele *users* i *posts*
2. Następnie w bazie danych tworzone są tabele *users* i *posts* wg podanych poniżej schematów
3. Do poszczególnych tabel dodawane są dane z wykorzystaniem możliwie optymalnej pod względem czasowym metody
4. Gdy dane są już na miejscu, następuje ich policzenie
5. Następnie wykonywane są 3 zapytania, a ich wyniki wypisywane na ekran

Każdy z wyżej wymienionych kroków skryptu jest opatrzony pomiarem czasu.

### Schematy tabel

**Tabela *users*:**
```
CREATE TABLE users(
	_Id BIGINT,
    _Reputation BIGINT,
    _CreationDate TIMESTAMP,
    _DisplayName VARCHAR(255),
    _LastAccessDate TIMESTAMP,
    _WebsiteUrl VARCHAR(255),
    _Location VARCHAR(255),
    _Views BIGINT,
    _UpVotes BIGINT,
    _DownVotes BIGINT,
    _AccountId BIGINT
);
```

**Tabela *posts*:**
```
CREATE TABLE POSTS(
	_Id BIGINT,
    _PostTypeId INT,
    _AcceptedAnswerId INT,
    _CreationDate TIMESTAMP,
    _Score BIGINT,
    _ViewCount BIGINT,
    _OwnerUserId BIGINT,
    _LastEditorUserId BIGINT,
    _LastEditDate TIMESTAMP,
    _LastActivityDate TIMESTAMP,
    _Title VARCHAR(500),
    _Tags VARCHAR(500),
    _AnswerCount BIGINT,
    _CommentCount BIGINT,
    _FavoriteCount BIGINT
);
```

### 1. Usunięcie (starych) danych z bazy

Czas trwania: 3 ms

### 2. Utworzenie tabel *users* i *posts*

| Tworzona tabela | Czas tworzenia |
|-----------------|----------------|
| users | 58 ms |
| posts | 29 ms|

### 3. Import danych do bazy PostgreSQL

Dane zostały wcześniej przekonwertowane z fromatu XML do formatu CSV.

| Tabela | Czas importu |
|--------|--------------|
| users | 173.01 ms |
| posts | 384.02 ms |

### 4. Zliczenie dodanych rekordów

| Tabela | Liczba rekordów | Czas zapytania |
|--------|-----------------|----------------|
| users | 6423 | 3 ms |
| posts | 9366 | 3 ms |


### 5. Zapytania do bazy danych

**Zapytania:**

1. 3 posty z najwyższym _Score
2. 3 ostatnio napisane posty
3. Użytkownicy z liczbą postów między 5 a 7

**Wyniki zapytania nr 1** *(3 posty z najwyższym _Score)*
```
SELECT _id, _score FROM posts ORDER BY _score DESC LIMIT 3;
```

| _Id | Score |
|-----|-------|
| 203 | 74 |
| 179 | 65 |
| 205 | 60 |

Czas trwania: 5 ms

**Wyniki zapytania nr 2** *(3 ostatnio napisane posty)*
```
SELECT _id, _creationdate FROM posts ORDER BY _creationdate DESC LIMIT 3;
```

| _Id | Date |
|-----|------|
| 15702 | datetime.datetime(2017, 3, 13, 19, 42, 31, 757000) |
| 15701 | datetime.datetime(2017, 3, 13, 17, 16, 45, 167000) |
| 15699 | datetime.datetime(2017, 3, 13, 11, 45, 13, 800000) |

Czas trwania: 6 ms

**Wyniki zapytania nr 3** *(Użytkownicy z liczbą postów między 5 a 7)*
```
SELECT u._accountid, COUNT(*) 
FROM users u 
JOIN posts p 
ON u._accountid = p._owneruserid 
GROUP BY u._accountid 
HAVING COUNT(*) BETWEEN 5 AND 7;
```

| _AccountId | Liczba postów |
|------------|---------------|
|10836 | 6 |

Czas trwania: 6 ms

### Podsumowanie:

| Nr zapytania | Czas trwania |
|--------------|--------------|
| 1 | 5 ms |
| 2 | 6 ms |
| 3 | 6 ms |


## MongoDB

Ta część zadania opierająca się o bazę MongoDB została wykonana w języku Python 
w wersji 3.6.0 z wykorzystaniem biblioteki pymongo w wersji 3.4.0.

Kod rozwiązania dostępny [tutaj](https://bitbucket.org/nosql2017/nosql/src/master/zad2/scripts/mongo.py). 

Skrypt składa się z następujących części:

1. Na wstępie z bazy danych usuwane są (o ile istnieją) kolekcje *users* i *posts*
2. Do poszczególnych kolekcji dodawane są dane z wykorzystaniem możliwie optymalnej pod względem czasowym metody.
Do czasu potrzebnego na import danych dodany jest czas potrzebny na wczytanie danych z pliku
i ich podstawową obróbkę (zamiana napisów na liczby lub daty). Osobny pomiar tego etapu byłby uciążliwy,
ponieważ mając na względzie złożoność pamięciową dla bardzo dużych zbiorów danych, zdecydowałem się
skorzystać z generatorów, zamiast przechowywania całego zbioru danych w pamięci
3. Gdy dane są już na miejscu, zostają policzone dokumenty w odpowiednich kolekcjach
4. Na końcu wykonywane są 3 agregacje, a ich wyniki wypisywane są na ekran

Każdy z wyżej wymienionych kroków skryptu jest opatrzony pomiarem czasu.


### 1. Usunięcie (starych) danych z bazy

Czas trwania: 112.01 ms

### 2. Import i konwersja danych

Dane zostały wcześniej przekonwertowane z fromatu XML do formatu CSV.

| Tabela | Czas importu |
|--------|--------------|
| users | 1.14 s |
| posts | 1.88 s |

### 3. Zliczenie dodanych dokumentów

| Tabela | Liczba rekordów | Czas zapytania |
|--------|-----------------|----------------|
| users | 6423 | 1.0 ms |
| posts | 9366 | 2.0 ms |


### 4. Agregacje

**Lista agregacji:**

1. 3 posty z najwyższym _Score
2. 3 ostatnio napisane posty
3. Użytkownicy z liczbą postów między 5 a 7

**Wyniki agregacji nr 1** *(3 posty z najwyższym _Score)*
```
[  
    {
        $sort: {
            _Score: -1
        }
    },  
    {
        $limit: 3
    },  
    {
        $project: {
            _id: 0, 
            _Id: 1, 
            _Score : 1
        }
    }   
]
```

| _Id | Score |
|-----|-------|
| 203 | 74 |
| 179 | 65 |
| 205 | 60 |

Czas trwania: 11.0 ms

**Wyniki agregacji nr 2** *(3 ostatnio napisane posty)*
```
[
    {
        $project: {
            _id: 0, 
            _Id: 1, 
            _CreationDate: 1
        }
    },
	{
        $sort: {
            _CreationDate: -1
        }
    },
    {
        $limit: 3
    }
]
```

| _Id | Date |
|-----|------|
| 15702 | datetime.datetime(2017, 3, 13, 19, 42, 31, 757000) |
| 15701 | datetime.datetime(2017, 3, 13, 17, 16, 45, 167000) |
| 15699 | datetime.datetime(2017, 3, 13, 11, 45, 13, 800000) |

Czas trwania: 17.0 ms

**Wyniki agregacji nr 3** *(Użytkownicy z liczbą postów między 5 a 7)*
```
[
	{
        $lookup: {
            from: "users", 
            localField: "_OwnerUserId", 
            foreignField: "_AccountId", 
            as: "X"
        }
    },  
	{
        $group: {
            _id: "$X._AccountId", 
            posts: { 
                $sum: 1 
            }
        }
    },   
    {
        $match : {
            posts: {
                $gte: 5, 
                $lte: 7
            }
        }
    },  
	{
        $sort: {
            posts: -1 
        }
    }, 
    {
        $project : {
            posts: 1
        }
    }
]
```

| _AccountId | Liczba postów |
|------------|---------------|
|10836 | 6 |

Czas trwania: 43.67 s

### Podsumowanie:

| Nr agregacji | Czas trwania |
|--------------|--------------|
| 1 | 11.0 ms |
| 2 | 17.0 ms |
| 3 | 43.67 s |


# Zestawienie wyników baz mongoDB i PostgreSQL

## Porównanie czasu usunięcia starych danych z bazy

| Baza danych | Czas usuwania |
|-------------|---------------|
| mongoDB | 112.01 ms |
| PostgreSQL | 3 ms |


## Porównanie czasu importu danych do bazy

| Tabela/kolekcja | mongoDB | PostgreSQL |
|-----------------|---------|------------|
| users | 1.14 s | 173.01 ms |
| posts | 1.88 s | 384.02 ms |


## Porównanie czasu zliczania wierszy/dokumentów w bazie

| Tabela/kolekcja | mongoDB | PostgreSQL |
|----|------------|----------------------|
| users | 1 ms |  	3 ms |
| posts | 2 ms |  	3 ms |


## Porównanie czasu wykonania zapytań/agregacji

| nr | mongoDB | PostgreSQL |
|----|---------|------------|
| 1  | 11 ms   | 5 ms |
| 2  | 17 ms   | 6 ms |
| 3  | 43.67 s | 6 ms |

### [Powrót do strony głównej](https://bitbucket.org/nosql2017/nosql/wiki/Home)